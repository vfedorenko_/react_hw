const initialState = {
    list: [
        { id: 0, title: "todo 1", done: false }
    ],
    count: 1
}

const Reducer = ( state=initialState, action ) => {
    switch ( action.type ) {
        case 'ADD_TO_LIST': 
        return{
            ...state,
            list: [ ...state.list, action.payload ],
            count: ++state.count
        }

        case 'REMOVE_FROM_LIST':
        return{
            ...state,
            list: state.list.filter( item => {
                return item.id !== action.payload
            } )
            
        }

        case 'DONE_ITEM':
        return{
            ...state,
            list: done( state, action.payload )
        }

        default: 
            return state;
    }
    return state;
}

const done = ( state, iditem ) => {
    
    let arr = state.list.map( item => {
        
        if (item.id === iditem) {
            console.log("done", item, iditem)
            item.done = true;
            console.log(item)
            return item;
            
        }
        console.log("undone", item, iditem)
        return item
    } ) 

    return arr;
}

export default Reducer;