import All from '../pages/all';
import Done from '../pages/done';
import Undone from '../pages/undone';
import NotFound from '../pages/NotFound';
import Root from '../pages/root';

const Routes = [
    {
        path: '/',
        component: Root,
        exact: true
    },
    {
        path: '/all',
        component: All,
        exact: true
    },
    {
        path: '/done',
        component: Done,
        exact: true
    },
    {
        path: '/undone',
        component: Undone,
        exact: true
    },
    {
        component: NotFound
    }
]

export default Routes;