import React from 'react'
import { connect } from 'react-redux';

const Done  = ( { list } ) => {

    return(
        <div>

            <h3> Done Todo</h3>
            
            <ul>
                {
                    list.map( (item) => (
                        <li 
                            key={ item.id }
                            hidden={ !item.done }
                        >
                            { item.title }
                        </li>
                    ))
                }
            </ul>
        </div>
    )

}

/*
        Redux
*/

const mapStateToProps = ( state, ownProps ) => {
    return {
        list: state.list
    }
};

export default connect( mapStateToProps )( Done );