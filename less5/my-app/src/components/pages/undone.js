import React, { Component } from 'react'
import { connect } from 'react-redux';

class Undone  extends Component {

    doneToDo = ( e ) => {
        console.log(e.target.id)
        const { doneToDoInList } = this.props;

        doneToDoInList ( +e.target.id )
    }

    render() {
        const { list } = this.props;
        const { doneToDo } = this;

        return(
            <div>

                <h3> UnDone Todo</h3>
                
                <ul>
                    {
                        list.map( (item) => (
                            <li 
                                key={ item.id }
                                hidden={ item.done }
                            >
                                { item.title }
                                <button 
                                    id={ item.id }
                                    onClick={ doneToDo }
                                >DONE</button>
                            </li>
                        ))
                    }
                </ul>
            </div>
        )
    }


}

/*
        Redux
*/

const mapStateToProps = ( state, ownProps ) => {
    return {
        list: state.list
    }
};

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    doneToDoInList: ( id ) => {
        console.log(id)
        dispatch({
            type: 'DONE_ITEM',
            payload: id
        })
    }
});

export default connect( mapStateToProps, mapDispatchToProps )( Undone );