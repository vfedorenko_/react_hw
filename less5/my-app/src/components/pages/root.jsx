import React from 'react';
import { Redirect } from 'react-router-dom';

const Root  = () => {
    return(
        <Redirect from="/" to="/all"/>
    )
}

export default Root;