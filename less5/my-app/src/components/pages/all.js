import React, { Component } from 'react'
import { connect } from 'react-redux';

class All  extends Component {
    state = {
        item: '',
        id: this.props.count
    }

    changeHandler = ( e ) => {
        this.setState({
            item: e.target.value
        })
    }

    addToDo = ( e ) => {
        const { addToList } = this.props;
        
        addToList( this.state.id, this.state.item );
        this.setState({
            id: this.state.id + 1
        })
    }

    removeIt = ( e ) => {
        console.log(e.target.id)
        const { removeFromList } = this.props;

        removeFromList ( +e.target.id )
    }

    render() {
        const { list } = this.props;
        const { item } = this.state;
        const { changeHandler, addToDo, removeIt } = this;

        return(
            <div>

                <h3> Add Todo</h3>
                <input 
                    onChange={ changeHandler } 
                    value={item}
                />
                <button onClick={addToDo}> Add Todo </button>

                <ul>
                    {
                        list.map( (item) => (
                            <li 
                                key={ item.id }
                                className={ item.done ? "done" : "undone" }
                            >
                                { item.title }
                                <button 
                                    id={ item.id }
                                    onClick={ removeIt }
                                >remove</button>
                            </li>
                        ))
                    }
                </ul>
            </div>
        )
    }


}

/*
        Redux
*/

const mapStateToProps = ( state, ownProps ) => {
    return {
        list: state.list,
        count: state.count
    }
};

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    addToList: ( id, title ) => {
        dispatch({
            type: 'ADD_TO_LIST',
            payload: { id, title: title, done: false }
        })
    },
    removeFromList: ( id ) => {
        dispatch({
            type: 'REMOVE_FROM_LIST',
            payload: id
        })
    }
});

export default connect( mapStateToProps, mapDispatchToProps )( All );