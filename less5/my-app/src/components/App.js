import React from 'react';
import Routes from './routes/routes';
import { Route, Switch, NavLink } from 'react-router-dom';

import './App.css';

const App = () => {

    return (
        <>
            <nav>
				<NavLink to="/all">All</NavLink>
				<NavLink to="/done">Done</NavLink>
				<NavLink to="/undone">Undone</NavLink>
            </nav>
			<Switch>
				{
					Routes.map( ( route, index ) => (
						<Route 
							key={index}
							{ ...route }
						/>
					))
				}
			</Switch>
        </>
    )
}

export default App;