export const GET_POST_REQ = 'GET_POST_REQ';
export const GET_POST_RES = 'GET_POST_RES';
export const GET_POST_ERROR = 'GET_POST_ERROR';
export const PROMISE = 'PROMISE';

export const getPostPromise = ( num ) => ( dispatch ) => {

    dispatch({
        type: PROMISE,
        actions: [ GET_POST_REQ, GET_POST_RES, GET_POST_ERROR ],
        promise:
            fetch('http://jsonplaceholder.typicode.com/posts/' + num)
                .then( res => res.json() )
    });

}