export const GET_USER_REQ = 'GET_USER_REQ';
export const GET_USER_RES = 'GET_USER_RES';
export const GET_USER_ERROR = 'GET_USER_ERROR';
export const PROMISE = 'PROMISE';

export const getUserPromise = ( num ) => ( dispatch ) => {

    dispatch({
        type: PROMISE,
        actions: [ GET_USER_REQ, GET_USER_RES, GET_USER_ERROR ],
        promise:
            fetch('http://jsonplaceholder.typicode.com/posts?userId=' + num)
                .then( res => res.json() )
    });

}