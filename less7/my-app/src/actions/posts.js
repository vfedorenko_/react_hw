export const GET_POSTS_REQ = 'GET_POSTS_REQ';
export const GET_POSTS_RES = 'GET_POSTS_RES';
export const GET_POSTS_ERROR = 'GET_POSTS_ERROR';
export const PROMISE = 'PROMISE';

export const getPostsPromise = () => ( dispatch ) => {

    dispatch({
        type: PROMISE,
        actions: [ GET_POSTS_REQ, GET_POSTS_RES, GET_POSTS_ERROR ],
        promise:
            fetch('https://jsonplaceholder.typicode.com/posts/')
                .then( res => res.json() )
    });


}