import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { getPostsPromise } from '../../actions/posts';


class Home extends React.Component {
    state = {
        num: 50
    }

    componentDidMount() {
        this.props.getAllPosts();
    }

    render() {
        console.log(this.props)
        let { num } = this.state;
        let { data } = this.props.posts;
        const { more } = this;

        return(
            <div>
                <h1>Home</h1>
                {
                    data.map( (post, index) => {
                        if ( index < num ) {
                            return (
                                <Link
                                    key={post.id}
                                    to={"/post/:" + post.id}
                                >
                                    <h6>{post.title}</h6>
                                    <p>{post.body}</p>
                                </Link>
                            )
                        }
                    } )
                }
                {
                    (data.length > num) &&  <button onClick={more}>Show more</button> 
                }
            </div>
        )
    }

    more = () => {
        let shift = this.state.num + 25;
        this.setState({
            num: shift
        })
        console.log(shift)
    }
}

const mapStateToProps = (state) => ({
    posts: state.postsReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    getAllPosts: () => {
        dispatch( getPostsPromise() );
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Home);