import React from 'react';
import { Switch, Route } from 'react-router-dom';
import UserId from './userid.js'


const User = () => (
    <Switch>
        <Route exact path="/user/:userid" component={UserId} />
    </Switch>
)

export default User;