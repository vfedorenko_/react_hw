import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { getUserPromise } from '../../actions/user';

class UserId extends React.Component {

    componentDidMount() {
        let id = this.props.match.params.userid.split(':')[1];
        console.log(id)
        this.props.getUser(id);
    }

    render() {
        const { loaded, data } = this.props.user;

        return(
            <>
                <h1>User</h1>
                
                <h2>User {this.props.match.params.userid}</h2>
                {
                    loaded && 
                        data.map( (post, index) => {
                            return (
                                    <Link
                                        key={post.id}
                                        to={"/post/:" + post.id}
                                    >
                                        <h6>{post.title}</h6>
                                        <p>{post.body}</p>
                                    </Link>
                                )
                        } )
                }
            </>
        )
    }
    
}

const mapStateToProps = (state) => ({
    user: state.userReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    getUser: (id) => {
        dispatch( getUserPromise(id) );
    }
})

export default connect( mapStateToProps, mapDispatchToProps )(UserId);