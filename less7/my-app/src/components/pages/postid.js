import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { getPostPromise } from '../../actions/post';
import { getCommentPromise } from '../../actions/comment';

class PostId extends React.Component {

    componentDidMount() {
        let id = this.props.match.params.itemid.split(':')[1];
        console.log(id)
        this.props.getPost(id);
    }

    loadComm = () => {
        let id = this.props.match.params.itemid.split(':')[1];
        console.log(id)
        this.props.getComment(id);
    }

    render() {
        const { loaded, data } = this.props.post;
        const { comment } = this.props;
        const { loadComm } = this;

        return(
            <>
                <h1>Post</h1>
                
                {
                    loaded ? <>
                        <Link 
                            key={data.id}
                            to={"/user/:" + data.userId}
                        >Show all user's posts</Link>
                        <h2>{data.title}</h2>
                        <p>{data.body}</p>
                    </> : <h6>Loading...</h6>
                }
                {
                    comment.loaded ? <ul>
                        {
                            comment.data.map( (item, index) => {
                                return (<li
                                    key={index}
                                >
                                    {item.body}
                                </li>)
                            } )
                        }
                    </ul> : <button onClick={loadComm}>Comments</button>
                }
                
            </>
        )
    }
    
}

const mapStateToProps = (state) => ({
    post: state.postReducer,
    comment: state.commentReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    getPost: (id) => {
        dispatch( getPostPromise(id) );
    },
    getComment: (id) => {
        dispatch( getCommentPromise(id) )
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(PostId);