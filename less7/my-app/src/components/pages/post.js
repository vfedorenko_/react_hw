import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PostId from './postid.js'


const Posts = () => (
    <Switch>
        <Route exact path="/post/:itemid" component={PostId} />
    </Switch>
)

export default Posts;