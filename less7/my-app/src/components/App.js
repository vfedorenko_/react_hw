import React, { Component } from 'react';
import './App.css';
import { Route, Switch, NavLink } from 'react-router-dom';

import { ROUTES } from './routes'

class App extends Component {

    render () {


        return (
            <>
                {/* <nav>
                    <NavLink to="/">All</NavLink>
                    <NavLink to="/done">Done</NavLink>
                    <NavLink to="/undone">Undone</NavLink>
                </nav> */}
                <Switch>
                {
                    ROUTES.map( ( route, index ) => (
                        <Route 
                            key={index}
                            { ...route }
                        />
                    ))
                }
                </Switch>
            </>
        );
    }
}

export default App;
