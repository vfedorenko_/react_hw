const logger = store => next => action => {
    console.log('log: ', action);
    return next(action);
}

export default logger;