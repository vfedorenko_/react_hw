import {
    GET_POST_REQ,
    GET_POST_RES
} from '../actions';

const postsInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: []
};

const postReducer = ( state = postsInitialState, action) => {
    switch( action.type ){

        case GET_POST_REQ: 
            return {
                ...state,
                loaded: false,
                loading: true
            }

        case GET_POST_RES:
            return{
                ...state,
                loaded: true,
                loading: false,
                data: action.payload
            }

        default:
            return state;
    }
}

export default postReducer;