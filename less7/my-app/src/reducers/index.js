import { combineReducers } from 'redux';

import postsReducer from './posts';
import postReducer from './post';
import commentReducer from './comment';
import userReducer from './user';

const reducer = combineReducers({
    postsReducer,
    postReducer,
    commentReducer,
    userReducer
});

export default reducer;