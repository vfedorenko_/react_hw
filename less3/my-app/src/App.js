import React, { Component } from 'react';
import { Toggler, TogglerItem } from './components/togler';
import Input from './components/input'; 
import MyForm from './components/form';
// import Plot from './components/hw/plot';
import ParentForm from './components/hw/parentform';

import './App.css';

class App extends Component {
	state = {
		activeToggler: "left",
		gender: 'male',
		layout: 'middle'
	}

	changeStatus = ( key, value ) => () => {
		console.log("ch status", key, value)
		this.setState({
			[key]: value
		});
	}

	inputChange = ( e ) => {
		console.log( e.target.value );
	}

	render = () => {
		// let { gender, layout } = this.state;
		// let { inputChange } = this

		return (
			<div className="App">

				{/* <Toggler
					name="gender"
					label="Choose gender"
					value={gender}
					changeStatus={this.changeStatus}
				>
					<TogglerItem value="male" label="Choose layout" />
					<TogglerItem name="female"/>
				</Toggler>
				<Toggler
					name="layout"
					label="Choose layout"
					value={layout}
					changeStatus={this.changeStatus}
				>
					<TogglerItem value="left" label="Choose layout" />
					<TogglerItem value="center"/>
					<TogglerItem value="right"/>
					<TogglerItem value="baseline"/>
				</Toggler> */}

				{/* <Input 
					name="Your name"
					type="text"
					placeholder="write your name here"
					handler={inputChange}
					contentMaxLength={16}
					contentLength={true}
				/>
				<Input 
					name="Your password"
					type="password"
					placeholder="write your password here"
					handler={inputChange}
					contentMaxLength={16}
					contentLength={true}
				/> */}

				{/* <MyForm /> */}

				{/* ****Home works:**** */}
				{/* <Plot /> */}
				
				<ParentForm />
			</div>
		);
	}
}

export default App;
