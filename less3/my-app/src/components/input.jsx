/*

Задание 2:

    Написать кастомный элемент инпута.
    У компонента нужно сделать возможность настраивать:
    - type
    - placeholder
    - value-
    - onChangeHandler
    - name
    - contentLength ( свойтво которое показывает счетчик сколько символов набрано в инпут)
    - contentMaxLength ( Максимальное кол-во симовлов )
    Результатом будет компонент,
    <label>
        <div>{name}</div>

        <input
            type={type}
            placeholder={placeholder}
            value={value}
            onChange={handler}
        />
    </label>

    PropTypes:
        type: Одно из значений: text, password, number, required
        placeholder: строка
        value: строка или любой
        handler: function, required
        contentLength: bool
        contentMaxLength: number
*/

import React from 'react';
import PropTypes from 'prop-types';

// class Input extends React.Component {
//     state = {check: true, ...this.props};

//     checked = () => {
//         let {contentLength, contentMaxLength, value} = this.state;

//         if (contentLength) {
//             if (contentMaxLength) {
//                 if (value.length > contentMaxLength) {
//                     console.error("Long Value")
//                 }
//             } else {
//                 console.warn("parameter contentMaxLength is required")
//             }
//         }
//     } 

//     render = () => {
//         let { type, name, value, placeholder, handler } = this.state;
    
//         return(
//             <label>
//                 <div>{name}</div>
//                 <input
//                     type={type}
//                     placeholder={placeholder}
//                     value={value}
//                     onChange={handler}
//                 />
//             </label>
//         );
//     }
// }

const Input = ({ type, name, value, placeholder, handler, contentLength, contentMaxLength }) => {
    const handle = (e) => {
        console.log(contentLength, contentMaxLength)
        if (contentLength) {
            if (contentMaxLength) {
                if (e.target.value.length > contentMaxLength) {
                    console.error("Long Value")
                }
            } else {
                console.warn("parameter contentMaxLength is required")
            }
        }
        handler(e);
    }

    return(
        <label>
			<div>{name}</div>
			<input
                name={name}
				type={type}
				placeholder={value || placeholder}
				value={value}
                onChange={handle}
			/>
		</label>
    );
};

Input.propTypes = {
    type: PropTypes.oneOf(['text', 'password', 'number']).isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    handler: PropTypes.func.isRequired,
    contentLength: PropTypes.bool,
    contentMaxLength: PropTypes.number
}

export default Input;