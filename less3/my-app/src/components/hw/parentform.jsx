import React from 'react';
import ImgUpLoad from './imgupload';

export default class ParentForm extends React.Component {
    state = {
        img: ''
    }

    handlePic = (img) => {
        this.setState({
            img: img
        });
    }

    render = () => {
        const { handlePic } = this;
        let { img } = this.state;

        return(
            <form>
                <ImgUpLoad
                    src={img}
                    onSrcChange={handlePic}
                />
            </form>
        )
    }
}