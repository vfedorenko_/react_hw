/*
    Задание 2.

    Написать компонент для кастомного подгрузчика картинки.
    У вас есть спрятаный <input type="file"> и лейбл с красивым плейсхолдером.

    При клике на плейсхолдер и выборе новой картинки заменять плейсхолдер
    новой картинкой которую человек загрузил.
    Для отображения превью использовать FileReader.

    Новую картинку нужно будет записывать в стейт родительского компонента
    через метод прокинутый через пропсы.

    ---
    
    Родительский компонент, это форма которая будет состоять из:

    - Кастомный инпут (cw)
    - Тоглера (cw)
    - Выбора картинки

*/

import React from 'react';

export default class ImgUpLoad extends React.Component {

    fileInput = React.createRef();
    imgReader = React.createRef();

    image = () => {
        let { imgReader } = this;
        let img = new Image();
        img.file = this.props.src;

        imgReader = new FileReader();
        imgReader.onload = ( (aImg) => (e) => aImg.src = e.target.result )(img);
        // imgReader.readAsDataURL(this.props.src);
    }

    componentDidUpdate(prevProps) {
        console.log(this.props.src, prevProps.src)
        if (this.props.src !== prevProps.src) {
            this.imgReader = this.image();
            // this.chart.update();
            console.log('done')
        }
    }
    
    onchange = ( e ) => {
        console.dir(e.target);
        if (e.target.value) {
            this.props.onSrcChange(this.fileInput.current.files[0].name);
        }
    }

    render = () => {
        const { onchange, fileInput, imgReader } = this;
        let { src } = this.props;

        return(
            <label>
                <input type="file" ref={fileInput} onChange={onchange} hidden/>
                {
                    src ? (
                        <img src={src} ref={imgReader} alt="upload"></img>
                    ) : (
                        <span>Upload Image</span>
                    )
                }
            </label>
        )
    }
}