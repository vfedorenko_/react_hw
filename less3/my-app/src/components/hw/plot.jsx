/*
    Задание 1.

    Используя механику Ref и библиотеку chartjs, сделать компонент,
    который построит график по следующим параметрам:

    И будет иметь кнопку "Randomize Data" который изменит dataset для текущего элемента.


    Пакет для установки:
    npm install chart.js --save

    Документация https://www.chartjs.org/docs/latest/developers/api.html
    Примеры: https://www.chartjs.org/samples/latest/
    С чего начать: https://www.chartjs.org/docs/latest/getting-started/

    1) Создайте структуре внутри компонента, для корректного отображения графика
    и прицепите к нему ref для доступа библиотеки. (Реф предварительно нужно создать внутри классового компонента);
    chartEl = React.createRef();
    <canvas ref={this.chartEl} />
    
    2) В жизненном цикле CDM проинициализируйте график с default настройками.
    Пример настроек по ссылке getting started и ниже:
    new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'My First dataset',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: [0, 10, 5, 2, 20, 30, 45]
            }]
        },
        options: {}
    });

    ctx в данном случае должен быть ссылкой на канвас, который вы получаете через ref.
    а результат выполнения данной функции, рекомендую записать в this.chart

    3) Замените в этом объекте конфигурации dataset[0].data на объект состояния, который вы будете
    рандомить и менять.

    4) Создайте кнопку и повесьте на нее метод который будет рандомить вам нужное количество 
    элементов массива в заданном диапазоне

    this.randomizeData = ( count, min, max ) => {...}
    [0, 10, 5, 2, 20, 30, 45]
    [14, 51, 38, 63, 12, 50, 63]
    [95, 17, 41, 19, 20, 19, 95]

    и будет присваивать получившееся значение в стейт компонента.

    5) После замены массива в состоянии, в методе CDU
    отловить этот момент и заменить данные уже в объекте графика и вызвать метод update для 
    его перерисовки

    Например так:
    this.chart.data.datasets[0].data = prevState.data;
    this.chart.update();
*/  

import React ,{ Component } from 'react';

const Chart = require('chart.js');

class Plot extends Component {

    chartEl = React.createRef();
    chart = {}

    state = {
        data: [0, 10, 5, 2, 20, 30, 45],
    }
    
    

    charts = () => {
        return new Chart(this.chartEl.current, {
            type: 'bar',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [{
                    label: 'My First dataset',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: this.state.data
                }]
            },
            options: {}
        });
    }
    
    componentDidMount() {
        console.log(this.chartEl)
        this.chart = this.charts();
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.chart.data.datasets[0].data === prevState.data)
        if (this.chart.data.datasets[0].data !== prevState.data) {
            this.chart = this.charts();
            // this.chart.update();
            console.log('done')
        }
    }

    random = ( e ) => {
        e.preventDefault();
        this.setState({
            data: random7()
        })
        // console.log(this.state.data)
    }

    render = () => {
        const { random } = this;

        return(
            <div>
                <canvas ref={this.chartEl}></canvas>
                <button onClick={random}>Randomize</button>
            </div>
        )
    }
}

export default Plot;

const randomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const random7 = () => {
    let data = [];
    for ( let i = 0; i < 7; i++ ) {
        data.push( randomInt( 0, 50 ) );
    }
    return data;
}