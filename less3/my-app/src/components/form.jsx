/*
    Задание 3:
    Написать форму которая будет состояить из двух ваших кастомных компонентов поля:
    - Имя
    - Пароль
    - Пол (переключалка)
    - Возраст
    - Layout (переключалка)
    - Любимый язык

    Результат:
    При отправке выводит в консоль все данные.
*/

import React, { Component } from 'react';
import { Toggler, TogglerItem } from './togler';
import Input from './input';


export default class MyForm extends Component {
    state = {
        name: '',
        password: '',
        gender: '',
        age: '',
        layout: '',
        language: '',
        status: 'unactive',
        pop: 'true'
    }

    handleInput =  ( e ) => {
        console.log('input')
        const name = e.target.name;
        const value = e.target.value;
        console.log(name, value);
        this.setState({
            [name]: value
        });
    }
    
    changeToggler = ( key, value ) => () => {
        console.log("ch status", key, value)
		this.setState({
			[key]: value
		});
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log( this.state );
    }

    render() {
        const { handleInput, handleSubmit, changeToggler} = this;
        let { gender, layout } = this.state;
        let { name, password, age, language } = this.state;
        return (
            <form onSubmit={handleSubmit}>
                <Input 
					name="name"
                    type="text"
                    value={name}
					placeholder="write your name here"
					handler={handleInput}
					contentMaxLength={16}
					contentLength={true}
				/>
				<Input 
					name="password"
					type="password"
                    value={password}
					placeholder="write your password here"
					handler={handleInput}
					contentMaxLength={16}
					contentLength={true}
				/>
                <Toggler
					name="gender"
					label="Choose gender"
					value={gender}
					changeStatus={changeToggler}
				>
					<TogglerItem value="male" label="Choose layout" />
					<TogglerItem name="female"/>
				</Toggler>
                <Input 
					name="age"
					type="number"
                    value={age}
					placeholder="write your age here"
					handler={handleInput}
				/>
                <Toggler
					name="layout"
					label="Choose layout"
					value={layout}
					changeStatus={changeToggler}
				>
					<TogglerItem value="left" label="Choose layout" />
					<TogglerItem value="center"/>
					<TogglerItem value="right"/>
					<TogglerItem value="baseline"/>
				</Toggler>
                <Input 
					name="language"
					type="text"
                    value={language}
					placeholder="write your favorite language"
					handler={handleInput}
				/>
                <button> Send </button>
            </form>
        )
    }
}