/*

    Задание 1:

    Написать кастомный компонент переключалки, который можно будет ре-использовать в форме.
    По примеру того, что мы разобрали в аудитории.

    Результатом должен стать компонент, который принимает в себя:
    - параметры переключалки отдельными дочерними компонентами
    - активное состояние
    - name
    - action который изменяет стейт роителя
        + бонус, action должен быть универсальным для нескольких переключалок.

    У компонента должна быть проверка PropTypes.
    - name: обязательное поле, строка
    - action: обязательное поле, функция
    - activeState: не обязательное поле, строка
    - children, не обязательное поле, реакт-компонент


    Тест: Добавить 2 элемента Toggler которые будут менять стейт родителя.
        Пусть это будут поля:
        gender > male,female
        layout > left, center, right, baseline

*/

import React from 'react';
import PropTypes from 'prop-types';

export class Toggler extends React.Component {


    render = () => {
        let { label, children, value, changeStatus, name } = this.props;

        return(
            <label>
                <span>{ label }</span>
                <div className="togglerContainer">
                    {
                        React.Children.map( children, (child) => {
                                if ( React.isValidElement( child ) ) {
                                    return React.cloneElement( child, {
                                        parentName: name,
                                        handler: changeStatus,
                                        active: child.props.value === value
                                    } )
                                } else {
                                    return null;
                                }
                            }
                        )
                    }
                </div>
            </label>
        )

    }
}

export const TogglerItem = ({name, value, parentName, active, handler}) => {
    return(
        <div className={
            active === true ? 
            "togglerItem active" :
            "togglerItem"
        }
        onClick={
        handler !== undefined ?
            handler( parentName, value ) :
            null
        }>
        {
            name || value
        }
        </div>
    )
}

Toggler.propTypes = {
    name: PropTypes.string.isRequired,
    changeStatus: PropTypes.func.isRequired,
    active: PropTypes.string,
    children: PropTypes.array,
};

// Toggler.propTypes = {
//     name: PropTypes.string,
//     handler: PropTypes.func.isRequired,
//     value: PropTypes.any,
//     children: PropTypes.element,
//     parentName: PropTypes.string.isRequired,
// };

export default {Toggler, TogglerItem}