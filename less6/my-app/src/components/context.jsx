import React, { Component } from 'react'


export const AUTH_CONTEXT = React.createContext();

export class ContextComponent extends Component {

    state = {
        auth: false,
        user: null,
        token: null
    }

    changeAuth = ( user ) => {
        this.setState({ 
            user,
            auth: true
         });
    }

    setToken = token => this.setState({ token })

    render() {
        const { auth } = this.state;
        const {state} = this;
        const { changeAuth, setToken } = this;

        return (
            <div>
                <AUTH_CONTEXT.Provider value={{state, changeAuth, setToken}}>
                    { this.props.children }
                </AUTH_CONTEXT.Provider>
            </div>
        )
    }
}

export default ContextComponent;