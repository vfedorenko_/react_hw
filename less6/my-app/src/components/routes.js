import regis from './pages/regis';
import login from './pages/login';
import privList from './pages/privlist';
import notfound from './pages/notfound';

const ROUTES = [
    {
        path: '/registration',
        component: regis,
        exact: true,
        privet: false
    },
    {
        path: '/login',
        component: login,
        exact: true,
        privet: false
    },
    {
        path: '/privatelist',
        component: privList,
        exact: true,
        privat: true
    },
    {
        component: notfound,
    }
]

export default ROUTES;