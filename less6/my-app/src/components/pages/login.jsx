import React from 'react';
import MyForm from './apps/logform';
import { NavLink } from 'react-router-dom';
import { AUTH_CONTEXT } from '../context';

class Login extends React.Component  {
    
    state = {
        auth: false,
        user: null
    }

    static contextType = AUTH_CONTEXT;

    loginUser = ( body ) => {
        fetch( `http://172.17.13.189:4004/auth/login`, {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify( body )
        })
        .then( res => {

            let token = res.headers.get('Authorization');
            if( token ){
                this.context.setToken( token );
            }

            return res.json();
        })
        .then( res => {
            let user = res.data.account;
            if( user ){
                console.log('LOG', this.context, user  )
                this.context.changeAuth( user );
            }
        });
    }

    handleUser = ( props ) => {
        this.loginUser( props.name, props.password );
    }

    render () {
        let { authkey } = this.state;

        return (
            <>
                <MyForm 
                    onSubmitMyForm={this.loginUser}
                />
                <NavLink to="/registration">Sign Up</NavLink>
            </>
        )
    }
}

export default Login;