import React, { Component } from 'react';
import Input from './input';


export default class MyForm extends Component {
    state = {
        email: 'test@tt.ua',
        password: '11111111'
    }

    handleInput =  ( e ) => {
        console.log('input')
        const name = e.target.name;
        const value = e.target.value;
        console.log(name, value);
        this.setState({
            [name]: value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log( this.state );
        this.props.onSubmitMyForm(this.state);
    }

    render() {
        const { handleInput, handleSubmit } = this;
        let { email, password } = this.state;
        return (
            <form onSubmit={handleSubmit}>
                <Input 
					name="email"
                    type="text"
                    value={email}
					placeholder="write your email here"
					handler={handleInput}
					contentMaxLength={16}
					contentLength={true}
				/>
				<Input 
					name="password"
					type="password"
                    value={password}
					placeholder="write your password here"
					handler={handleInput}
					contentMaxLength={16}
					contentLength={true}
				/>
                <button> Send </button>
            </form>
        )
    }
}