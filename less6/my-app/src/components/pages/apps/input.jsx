import React from 'react';
import PropTypes from 'prop-types';

const Input = ({ type, name, value, placeholder, handler, contentLength, contentMaxLength }) => {
    const handle = (e) => {
        console.log(contentLength, contentMaxLength)
        if (contentLength) {
            if (contentMaxLength) {
                if (e.target.value.length > contentMaxLength) {
                    console.error("Long Value")
                }
            } else {
                console.warn("parameter contentMaxLength is required")
            }
        }
        handler(e);
    }

    return(
        <label>
			<div>{name}</div>
			<input
                name={name}
				type={type}
				placeholder={value || placeholder}
				value={value}
                onChange={handle}
			/>
		</label>
    );
};

Input.propTypes = {
    type: PropTypes.oneOf(['text', 'password', 'number', 'email']).isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    handler: PropTypes.func.isRequired,
    contentLength: PropTypes.bool,
    contentMaxLength: PropTypes.number
}

export default Input;