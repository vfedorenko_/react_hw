import React, { Component } from 'react';
import Input from './input';


export default class MyForm extends Component {
    state = {
        name: '',
        password: '',
        password_confirm: '',
        email: ''
    }

    handleInput =  ( e ) => {
        console.log('input')
        const name = e.target.name;
        const value = e.target.value;
        console.log(name, value);
        this.setState({
            [name]: value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log( this.state );
        this.props.onSubmitMyForm(this.state);
    }

    render() {
        const { handleInput, handleSubmit } = this;
        let { name, password, password_confirm, email } = this.state;
        return (
            <form onSubmit={handleSubmit}>
                <Input 
					name="name"
                    type="text"
                    value={name}
					placeholder="write your name here"
					handler={handleInput}
					contentMaxLength={16}
					contentLength={true}
				/>
				<Input 
					name="password"
					type="password"
                    value={password}
					placeholder="write your password here"
					handler={handleInput}
					contentMaxLength={16}
					contentLength={true}
				/>
                <Input 
					name="password_confirm"
					type="password"
                    value={password_confirm}
					placeholder="confirm your password here"
					handler={handleInput}
					contentMaxLength={16}
					contentLength={true}
				/>
                <Input 
					name="email"
					type="email"
                    value={email}
					placeholder="write your email here"
					handler={handleInput}
				/>
                <button> Send </button>
            </form>
        )
    }
}