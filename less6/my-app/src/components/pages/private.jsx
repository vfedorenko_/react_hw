import React from 'react'
import {
    Redirect,
    Route
} from 'react-router-dom';
import { AUTH_CONTEXT } from '../context';

const PrivateRoute = ({ auth, location, ...props}) => {
    return (
        <AUTH_CONTEXT.Consumer>
            {
                user => {
                    console.log('private user', user)
                    if ( user.state.auth ) {
                        return ( <Route {...props} /> );
                    } else {
                        return (
                            <Redirect 
                                to={{
                                    pathname: "/login",
                                    state: {
                                        from: location.pathname
                                    }
                                }}
                            />
                        )
                    }
                }
            }
        </AUTH_CONTEXT.Consumer>
    )
}

export default PrivateRoute;