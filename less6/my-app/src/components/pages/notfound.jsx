import React from 'react';

const notfound = () => {
    return(
        <h1>Not Found: 404</h1>
    )
}

export default notfound;