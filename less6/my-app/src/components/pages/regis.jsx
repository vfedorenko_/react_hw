import React from 'react';
import MyForm from './apps/regform';
import { NavLink } from 'react-router-dom';

class Regis extends React.Component {
    state = {
        registered: false,
        user: {}
    }

    registerUser = ( body ) => {
 
        fetch( `http://172.17.13.189:4004/auth/registration`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        })
        .then( res => {
            console.log( "res", res );
    });
    }
    
    handleUser = ( props ) => {
        this.setState({
            user: props
        });
        this.registerUser();
        console.log( "res", this.state );
    }

    render () {
        return(
            <>
                <MyForm 
                    onSubmitMyForm={this.registerUser}
                />
                <NavLink to="/login">Login</NavLink>
                {/* registered && <NavLink to="/login">Login</NavLink> */}
            </>
        )
    }
}

export default Regis;



