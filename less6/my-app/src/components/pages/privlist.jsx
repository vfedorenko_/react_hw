import React from 'react';
import { AUTH_CONTEXT } from '../context';

class priveList extends React.Component {
    static contextType = AUTH_CONTEXT;

    state = {
        list: [],
        loaded: false,
        token: this.context.state
    }

    componentDidMount() {
        fetch(`http://172.17.13.189:4004/list`, {
            method: 'GET',
            headers: {
                Authorization: this.context.state.token
            }
        })
        .then( res => res.json())
        .then( res => this.setState({list: res.data.users, loaded: true}));
    }
    
    render() {
        const { list , loaded } = this.state;

        return(
            <>
                <h1>User List</h1>
                <ul>
                    {
                        loaded &&  list.map ( (itm, index) => {
                            return(<li key={index}>{itm.email}</li>)
                        } )
                    }
                </ul>
            </>
            
            
        )
    }
}

export default priveList;