import React from 'react';
import { BrowserRouter, Route, Switch, NavLink } from 'react-router-dom';
import PrivateRoute from './components/pages/private';
import ROUTES from './components/routes';
import Authorisation from './components/context';
import './App.css';

class App extends React.Component {
    state = {
        auth: false
    }

    render() {
        let { auth } = this.state;

        return (
            <BrowserRouter>
                <Authorisation >
                    <header>
                        <NavLink to="/registration"> Reg </NavLink>
                        <NavLink to="/login"> Login  </NavLink>
                        <NavLink to="/privatelist"> List </NavLink>
                    </header>
                    <Switch>
                        {/* <Redirect from="/" to="/login"/> */}
                        {
                            ROUTES.map( ( route, index ) => {
                                if ( route.privat ) {
                                    return(
                                        <PrivateRoute 
                                            key={index}
                                            { ...route }
                                        />
                                    )
                                } else {
                                    return (<Route 
                                        key={index}
                                        { ...route }
                                    />)
                                }
                            })
                        }
                    </Switch>
                </Authorisation>	
            </BrowserRouter>
        );
    }
}

export default App;
