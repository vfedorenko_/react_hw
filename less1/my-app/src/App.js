import React, { Component } from 'react'
import guestData from './guests.json';

const modGuests = guestData.map( guest => {
  guest.arrived = false;
  return guest;
} );

const ListItem = ( { guest, handler } ) => (
  <li
    data-index={guest.index}
    style={{ background: guest.arrived ? 'green' : 'transparent' }}
  >
    <p>
      { guest.name } works at { guest.company }.<br />
      { guest.phone }.<br />
      { guest.address }
    </p>
    <button
      data-index={guest.index}
      onClick={ handler }
    >
      Has Arrived
    </button>
  </li>
);

class App extends Component {

  state = {
    guests: modGuests,
    loaded: true,
    whoGuest: []
  }

  filterItems = ( e ) => {
    console.log(e.target.value);
    const who = e.target.value;

    if ( who ) { 
      this.setState({
        whoGuest: this.state.guests.filter( guest => guest.name === who ),
        loaded: false
      }) 
    } else {
      this.setState({
        loaded: true
      })
    }
  }

  comed = ( e ) => {
    console.log('arraved', e.target.dataset.index);
    const index = Number(e.target.dataset.index);

    let changedGuests = this.state.guests.map( guest => {
      if ( guest.index === index ) {
        guest.arrived = !guest.arrived;
      }
      return guest;
    })

    this.setState({
      guests: changedGuests
    })
  }

  render = () => {
    const { filterItems, comed } = this;
    const { guests, loaded, whoGuest } = this.state;

    

    return (
      <div>
        <h1>The Guests List</h1>
        <input onInput={filterItems} />
        {
          // If the list of all guests
          loaded ? (
            <ul>
              {
                guests.map( item => ( 
                  <ListItem
                    key={item.index}
                    guest={item}
                    handler={comed}
                  />
                ) )
              }
            </ul>
          // And other way:
          ) : (
            ( whoGuest.length > 0 ) ? 
            // if the single user was founded
            (
              <ul>
                {
                  whoGuest.map( item => ( 
                    <ListItem
                      key={item.index}
                      guest={item}
                      handler={comed}
                    />
                  ) )
                }
              </ul>
            ) : (
            // If no one was founded showing the message
              <p>Nothing was found</p>
            )
          )
        }
      </div>
    )
  }
}



export default App;
