/*
	2. Приложение по поиску фильмов
	Используя апи: https://developers.themoviedb.org/3/getting-started/introduction
	Написать приложение по поиску фильмов и сортировкой их по каталогам.
	
	Нужно реализовать следующие страницы и модули:
	- Шапка с поиском: глобальный модуль который отображается на всех страницах.
		-> Подмодуль: При вводе названия отправляется запрос на поиск соответствующего фильма.
		-> Подмодуль: Меню
	- Главная страница: выводит последние фильмы
	- Страница фильма: Краткая информация о фильме - постер, название, бюджет, дата выхода и т.д
		-> Подмодуль: Показать похожие фильмы
		-> Подмодуль: Поставить фильму рейтинг (Записывает ваш рейтинг в localstorage)
	- Страница каталога: Показывает фильмы определенного жанра
*/
	
	
	
