export default {
    uk: {
        'search.enter': 'Введіть слово для пошук...',
        'search.button': 'Шукати',
        'search.title': 'Знайдено для запиту ',
    },
    en: {
        'search.enter': 'Enter search term...',
        'search.button': 'Search',
        'search.title': 'Was founded for ',
    },
    it: {
        'search.enter': 'Inserisci le parole da cercare...',
        'search.button': 'Trovare',
        'search.title': 'E stato trovato da ',
    },
}