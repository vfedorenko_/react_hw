export default {
    uk: {
        'pagination.first': '|<<',
        'pagination.previouse': '<<',
        'pagination.next': '>>',
        'pagination.last': '>>|',
    },
    en: {
        'pagination.first': '|<<',
        'pagination.previouse': '<<',
        'pagination.next': '>>',
        'pagination.last': '>>|',
    },
    it: {
        'pagination.first': '|<<',
        'pagination.previouse': '<<',
        'pagination.next': '>>',
        'pagination.last': '>>|',
    },
}