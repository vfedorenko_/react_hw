import navigation from './navigation';
import home from './home';
import genres from './genres';
import catalog from './catalog';
import card from './card';
import search from './search';
import config from './config';

const TranslationsArray = [
    navigation, 
    home,
    genres,
    catalog,
    card,
    search,
];

let ConnectedTranslations = {};
config.supprotedLangs.forEach( lang => ConnectedTranslations[lang] = {} );

TranslationsArray.forEach( translationObject => {
    config.supprotedLangs.forEach( lang => {
        ConnectedTranslations[lang] = {
        ...ConnectedTranslations[lang],
        ...translationObject[lang]
        };
    })
});

export default ConnectedTranslations;
