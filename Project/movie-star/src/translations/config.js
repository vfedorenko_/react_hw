export default {
    defaultLanguage: 'uk',
    supprotedLangs: ['uk', 'en', 'it']
}