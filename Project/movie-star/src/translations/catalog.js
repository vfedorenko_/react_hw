export default {
    uk: {
        'catalog.list': 'Каталог фільмів',
    },
    en: {
        'catalog.list': 'The List of Films',
    },
    it: {
        'catalog.list': 'Lista dei film',
    },
}