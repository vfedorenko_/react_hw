import { axios_movielist } from '../helpers/axiosnight';
import { API_KEY, LANG } from '../constants';

export const GET_FILM_REQ = 'GET_FILM_REQ';
export const GET_FILM_RES = 'GET_FILM_RES';
export const GET_FILM_ERROR = 'GET_FILM_ERROR';
export const PROMISE = 'PROMISE';

export const getMoviePromise = ( id, lang ) => ( dispatch ) => {
    console.log('actions', API_KEY, id, lang )
    dispatch({
        type: PROMISE,
        actions: [ GET_FILM_REQ, GET_FILM_RES, GET_FILM_ERROR ],
        promise: axios_movielist.get(`/movie/${id}${API_KEY}${LANG}${lang}`)
    })
    
}