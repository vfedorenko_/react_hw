import { axios_movielist } from '../helpers/axiosnight';
import { API_KEY, LANG } from '../constants';

export const GET_GENRE_LIST_REQ = 'GET_GENRE_LIST_REQ';
export const GET_GENRE_LIST_RES = 'GET_GENRE_LIST_RES';
export const GET_GENRE_LIST_ERROR = 'GET_GENRE_LIST_ERROR';
export const PROMISE = 'PROMISE';

export const getGanreListPromise = ( lang ) => ( dispatch ) => {
    dispatch({
        type: PROMISE,
        actions: [ GET_GENRE_LIST_REQ, GET_GENRE_LIST_RES, GET_GENRE_LIST_ERROR ],
        promise: axios_movielist.get(`/genre/movie/list${API_KEY}${LANG}${lang}`)
    })
    
}