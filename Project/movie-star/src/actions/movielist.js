import { axios_movielist } from '../helpers/axiosnight';
import { API_KEY, GENRE, PAGE, SEARCH, LANG } from '../constants';

export const GET_MOVIE_LIST_REQ = 'GET_MOVIE_LIST_REQ';
export const GET_MOVIE_LIST_RES = 'GET_MOVIE_LIST_RES';
export const GET_MOVIE_LIST_ERROR = 'GET_MOVIE_LIST_ERROR';
export const PROMISE = 'PROMISE';

export const getMovieListPromise = (where, page, genre, query, lang) => ( dispatch ) => {
    console.log(where, page, genre, query, lang)
    let str = `${where}${API_KEY}`;
    if (genre) {
        str += `${GENRE}${genre}`;
    }
    if (page) {
        str += `${PAGE}${page}`;
    }
    if (query) {
        str += `${SEARCH}${query}`;
    }
    if (lang) {
        str += `${LANG}${lang}`;
    }
    
    console.log( 'actions', str )
    dispatch({
        type: PROMISE,
        actions: [ GET_MOVIE_LIST_REQ, GET_MOVIE_LIST_RES, GET_MOVIE_LIST_ERROR ],
        promise: axios_movielist.get(str)
    })
}