import { PROMISE } from '../../actions';

const promise = (store) => (next) => (action) => {
    if (action.type !== PROMISE) {
        return next(action);
    } else {
        const [startAct, successAct, failureReq] = action.actions;
        store.dispatch({
            type: startAct
        })
        return action.promise.then( res => store.dispatch({
            type: successAct,
            payload: res.data
        })), error => store.dispatch({ 
            type: failureReq,
            error: error
        })
    }
}

export default promise;