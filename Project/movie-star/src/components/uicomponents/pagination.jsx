import React from 'react';

const Paginator = ( props ) => {
    // console.log(props)
    
    const handl = ( iter ) => ( e ) => {
        let page = {
            page: iter,
            tot: props.total,
            par: props.parent
        }

        props.handler( page );
    } 

    const { page, total } = props

    return(
        <div className="paginator">
            {
                (page > 1) && <button onClick={handl(1)}>begin</button>
            }
            {
                (page > 2) && <button onClick={handl(page - 1)}>prev</button>
            }

            {
                (page > 3) && <button disabled>...</button>
            }
                
            {
                (page > 1) && <button onClick={handl(page - 1)}>{page - 1}</button>
            }
                <button onClick={handl(page)} disabled>{page}</button>
            {
                (page < total) && <button onClick={handl(page + 1)}>{page + 1}</button>
            }
            
            {
                (page < total - 2) && <button disabled>...</button>
            }
                
            {
                (page < total - 1) && <button onClick={handl(page + 1)}>next</button>
            }
            {
                (page < total) && <button onClick={handl(total)}>last</button>
            }
        </div>
    )
}

export default Paginator;