import React from 'react';
import { connect } from 'react-redux';

import L10nLink from '../common/L10nLink'

import Loader from '../uicomponents/loader';

// class SideBar extends React.PureComponent {

//     render() {
//         const { loaded, data } = this.props.list;

//         return (
//             <div className="col-2 sidebar">
//                 <h3>{this.props.title}</h3>
//                 {
//                     (data.genres) ? data.genres.map( itm => (
//                         <L10nLink key={itm.id} to={"/ganre/"+ itm.id}>{itm.name}</L10nLink>
//                     ) ) :
//                     <Loader />
//                 }
//             </div>
//         )
//     }
// }

const SideBar = (props) => {
    const { loaded, data } = props.list;

    return (
        <div className="col-2 sidebar">
            <h3>{props.title}</h3>
            {
                (data.genres) ? data.genres.map( itm => (
                    <L10nLink key={itm.id} to={"/ganre/"+ itm.id}>{itm.name}</L10nLink>
                ) ) :
                <Loader />
            }
        </div>
    )
}

const mapStateToProps = (state) => ({
    list: state.ganreListReducer
});

const mapDispatchToProps = ( dispatch ) => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);