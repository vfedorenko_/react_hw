import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import L10nLink from '../common/L10nLink';

class SearchBar extends React.Component {
    state ={
        value: ''
    }

    handler = (e) => {
        e.preventDefault();
        this.setState({
            value: e.target.value
        });
        console.log(this.state.value);
    }

    render = () => {
        const { value } = this.state;
        const { intl } =this.props;

        return (
            <form action="#" method='GET' className="search-form">
                <label htmlFor="search" className="screen-reader-text">Search</label>
                <input id='search' type="search" className='search-input' placeholder={intl.formatMessage({ id: 'search.enter' })} value={value} onChange={this.handler}/>
                <L10nLink className="search-button" to={"/search/" + value}><FormattedMessage id="search.button"/></L10nLink>
            </form>
        )
    }
}

export default injectIntl(SearchBar);