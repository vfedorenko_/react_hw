import React from 'react';
import { loadPartialConfig } from '@babel/core';

const Loader = () => {
    return(
        <h3>Loading...</h3>
    )
}

export default Loader;