import React from 'react';

import ChL10nLink from '../common/ChL10Link'

import config from '../../translations/config';

const LangSwitcher = ( props ) => {
    let path = props.path.split('/');
    console.log(path);
    path = path.filter( (item, index) => index !== 1);
    console.log(path);
    path = path.join('/');
    console.log(path);

    return(
        <>
            {
                config.supprotedLangs.map( item => (
                        <ChL10nLink key={item} to={path} newlang={item}> {item} </ChL10nLink>
                    )
                )
            }
        </>
    )
}

export default LangSwitcher;