import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

const Card = ( props ) => {
    return(
        <div className="card">
            <div className="card-header">
                <h5>{props.title}</h5>
                <p><FormattedMessage id="card.releas"/> {props.release_date}</p>
                <p><FormattedMessage id="card.pop"/> {props.popularity}</p>
            </div> 
            <img className="card-img-top" src={props.src} />
            <div className="card-body">
                <h6 className="card-title"><FormattedMessage id="card.original"/>{props.original_title}</h6>
                <p><FormattedMessage id="card.title"/>{props.title}</p>
                <p><FormattedMessage id="card.vote"/> {props.vote_average}</p>
            </div>
        </div>
    )
}

export default injectIntl(Card);