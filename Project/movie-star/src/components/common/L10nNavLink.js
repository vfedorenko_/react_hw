import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

const L10nNavLink = ( { to, lang, children, ...props } ) => {
    let langPath = '';

    if ( typeof(to) === 'object' ) {
        langPath = {
            ...to,
            pathname: `/${lang}${to.pathname}`
        }
    } else {
        langPath = `/${lang}${to}`;
    }

    return (
        <NavLink 
            to={langPath}
            {...props}
        >
            { children }
        </NavLink>
    )
}

const mapStateToProps = ( state ) => ({
    lang: state.l10nReducer.lang
})

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(L10nNavLink);