import React from 'react';
import { Router } from 'react-router-dom';

import config from '../../translations/config';

const L10nRouter = ( { path, lang, children, ...props } ) => {
    
    return (
        <Router 
            path={`/:locale(${config.supprotedLangs.join('|')})${path}`}
            {...props}
        >
            { children }
        </Router>
    )

}

export default L10nRouter;