import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { CHANGE_LANG } from '../../reducers/L10nReducer';

const ChL10nLink = ( { to, newlang, changeLang, children, ...props } ) => {
    let langPath = '';
    changeLang(newlang)

    if ( typeof(to) === 'object' ) {
        langPath = {
            ...to,
            pathname: `/${newlang}${to.pathname}`
        }
    } else {
        langPath = `/${newlang}${to}`;
    }

    return (
        <Link 
            to={langPath}
            {...props}
        >
            { children }
        </Link>
    )
}

const mapStateToProps = ( state ) => ({
    // lang: state.l10nReducer.lang
})

const mapDispatchToProps = ( dispatch ) => ({
    changeLang: ( lang ) => {
        dispatch({ type: CHANGE_LANG, lang: lang });
    }
});

export default connect(null, mapDispatchToProps)(ChL10nLink);