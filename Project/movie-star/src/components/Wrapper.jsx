import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import intlComponent from './intl.container';

import store from '../redux/store';

import config from '../translations/config';

const SupportedLangs = config.supprotedLangs.join('|');

const Wrapper = () => (
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route path={`/:locale(${SupportedLangs})`} component={intlComponent} />
                <Redirect to={`${config.defaultLanguage}`} />
            </Switch>
        </BrowserRouter>
    </Provider>
);

export default Wrapper;