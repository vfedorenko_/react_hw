import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

import Catalog from './catalog';

const Search = ({match}) => {
    return (
        <>
            <h2><FormattedMessage id="search.title"/> {match.params.what}</h2>
            <Catalog 
                req="/search/movie" 
                query={match.params.what} 
                lang={match.params.langPath} 
                gen={null}
            />
        </>
    )
}

export default injectIntl(Search);