import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

const NotFound  = () => {
    return(
        <h1>Not found: 401</h1>
    )
}

export default injectIntl(NotFound);