import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

import Catalog from './catalog';

const Main = ({match}) => {
    return(
        <>
            <h2><FormattedMessage id="home.title"/></h2>
            <Catalog 
                req="/movie/popular" 
                gen={null} 
                query={null} 
                lang={match.params.langPath}
            />
        </>
    )
}

export default injectIntl(Main);