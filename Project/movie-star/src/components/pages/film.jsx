import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage, injectIntl } from 'react-intl';

import L10nLink from '../common/L10nLink';

import { getMoviePromise } from '../../actions/film';

import Loader from '../uicomponents/loader';

class Film extends React.Component {

    componentDidMount() {
        const { id, langPath } = this.props.match.params
        console.log( id, langPath );
        this.props.getFilm( id, langPath );
    }

    render () {
        const { film } = this.props;
        // console.log(film);

        return (
            <>
                {
                    film.loaded 
                        ? <div className="card">
                            <div className="card-header">
                                <h3>{film.data.title}</h3>
                                <p><FormattedMessage id="card.releas"/>{film.data.release_date}</p>
                                <p><FormattedMessage id="card.pop"/>{film.data.popularity}</p>
                            </div> 
                            <div className="row no-gutters">
                                <div className="col-md-1"></div>
                                <div className="col-md-4">
                                    <img className="card-img" src={"https://image.tmdb.org/t/p/w500/" + film.data.poster_path} />
                                </div>
                                <div className="col-md-6">
                                    <div className="card-body">
                                        <h5 className="card-title"><FormattedMessage id="card.original"/>{film.data.original_title}</h5>
                                        <h6 className="card-title"><FormattedMessage id="card.title"/>{film.data.title}</h6>
                                        <p><FormattedMessage id="card.vote"/>{film.data.vote_average}</p>
                                        <p><FormattedMessage id="card.overview"/></p>
                                        <p>{film.data.overview}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="card-footer text-muted">
                                {
                                    film.data.genres.map( item => {
                                        return(
                                            <L10nLink key={item.id} to={"/ganre/"+ item.id}>{item.name}</L10nLink>
                                        )
                                    } )
                                }
                            </div>
                        </div>
                        : <Loader />
                }
            </>
        )
    }
    
}

const mapStateToProps = (state) => ({
    film: state.filmListReducer,
    genre: state.ganreListReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    getFilm: ( id, lang ) => {
        dispatch( getMoviePromise( id, lang ) );
    }
})

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Film));