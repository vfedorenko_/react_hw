import React from 'react';

import Catalog from './catalog';

const GanreId = ({ location, match }) => {
    let ganre = location.pathname.split('/')[3];
    console.log(ganre)

    return(
        <Catalog 
            req={'/discover/movie'} 
            gen={ganre} 
            query={null} 
            lang={match.params.langPath}
        />
    )
} 

export default GanreId;