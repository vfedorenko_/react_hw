import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { FormattedMessage, injectIntl } from 'react-intl';

import SideBar from '../uicomponents/sidebar';
import GanreId from './ganreid'

// const REQ = React.createContext({ data: '', setData: (val) => {data: val} });

const Ganre = () => {
    
    return(
        <>
            <div className="row">
            
                <SideBar title={<FormattedMessage id="genres.title"/>}/>
                
                <Switch>
                    <Route exact path="/:langPath/ganre/:id" component={GanreId}/>
                </Switch>
            </div>
        </>
        
    )
}

export default injectIntl(Ganre);