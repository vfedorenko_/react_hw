import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage, injectIntl } from 'react-intl';

import L10nLink from '../common/L10nLink';

import { getMovieListPromise } from '../../actions/movielist';
import { SET_PAGE } from '../../reducers/pagination';

import Paginator from '../uicomponents/pagination';
import Card from '../uicomponents/card';
import Loader from "../uicomponents/loader";

class Container extends React.Component {
    state = {
        page: this.props.page.page,
        gen: this.props.gen
    }

    componentDidMount = () => {
        console.log(this.state, this.props)
        const { req, gen, query, lang, getAllFilms } = this.props;
        
        if (req) {
            this.setState({
                gen: gen,
                page: 1
            })
            getAllFilms(req, this.state.page, this.state.gen, query, lang);
        }
        
    }

    componentDidUpdate(prevProps, prevState) {
        const { req, gen, query, lang, getAllFilms } = this.props;
        console.log(req, this.state.page, this.state.gen, gen, query, lang)

        if (this.state.gen !== gen) {
            this.setState({
                gen: gen,
                page: 1
            })
            getAllFilms(req, this.state.page, this.state.gen, query, lang);
        }
        if (prevState.page !== this.state.page) {
            getAllFilms(req, this.state.page, this.state.gen, query, lang);
        }

    }

    onChangePage = ( newStoreState ) => {
        // console.log( newStoreState );

        this.setState({
            page: newStoreState.page
        });
        this.props.setPage( newStoreState );

    }

    render() {

        const { list, gen, req } = this.props;
        const { page } = this.state;
        const { onChangePage } = this;

        // console.log(list)

        return (
            <div className="col">
                <h2><FormattedMessage id="catalog.list"/></h2>
                <div className="catalog">
                    {
                        list.loaded ? list.data.results.map( item => {
                            // console.log(item)
                            return(
                                <L10nLink key={item.id} to={"/"+ item.id}>
                                    <Card 
                                        key={item.id}
                                        src={"https://image.tmdb.org/t/p/w500/" + item.poster_path }
                                        {...item}
                                    />
                                </L10nLink>
                            )
                        } ) : <Loader />
                    }
                </div>
                {
                    list.loaded 
                        ? <Paginator parent={gen || req} page={page} total={list.data.total_pages} handler={onChangePage}/>
                        : <Loader />
                }
            </div>
        )

    }
    
}

const mapStateToProps = (state) => ({
    list: state.movieListReducer,
    page: state.pageReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    getAllFilms: (url, page, genre, query, lang) => {
        dispatch( getMovieListPromise(url, page, genre, query, lang) );
    },
    setPage: (param) => {
        console.log(param)
        dispatch({
            type: SET_PAGE,
            payload: {
                page: param.page,
                who: param.par,
                total: param.tot
            }
        })
    }
})

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Container));