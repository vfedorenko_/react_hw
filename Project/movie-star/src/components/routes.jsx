import NotFound from './pages/notfound';
import Main from './pages/main';
import Ganre from './pages/ganre';
import Film from './pages/film';
import Search from './pages/search';

export const ROUTES = [
    {
        path: '/:langPath/',
        component: Main,
        exact: true
    },
    {
        path: '/:langPath/ganre',
        component: Ganre,
        exact: false
    },
    {
        path: '/:langPath/:id',
        component: Film,
        exact: true
    },
    {
        path: '/:langPath/search/:what',
        component: Search,
        exact: true
    },
    {
        component: NotFound
    }
]

export default ROUTES;