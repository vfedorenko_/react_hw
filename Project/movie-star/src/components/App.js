import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { FormattedMessage, injectIntl } from 'react-intl';

import { ROUTES } from './routes';
import { getGanreListPromise } from '../actions';
import L10nNavLink from './common/L10nNavLink';
import LangSwitcher from './uicomponents/langswitcher';
import SearchBar from './uicomponents/searchbar';

import favicon2 from './images/favicon_red.ico?v2';
import './App.css';

class App extends React.PureComponent {
    componentDidMount() {
        console.log(this.props)
        if (!this.props.list.loaded) {
            console.log(this.props.list.loaded)
            this.props.getAllFilms(this.props.match.params.locale);
        }
        
    }

    render() {
        const { pathname } = this.props.location;

        return (
            <>
                <Helmet>
                    <title> Movie Star </title>
                    <link rel="shortcut icon" href={favicon2}/>
                </Helmet>
                <div className={"App container"}>
                    <header className="row">
                        {/* <div className="col-2"></div> */}
                        <nav className="col">
                            <L10nNavLink to="/"><FormattedMessage id="navigation.home"/></L10nNavLink>
                            <L10nNavLink to="/ganre"><FormattedMessage id="navigation.genres"/></L10nNavLink>
                        </nav>
                        <SearchBar />
                        {/* <div className="col-2">
                            <LangSwitcher path={pathname} />
                        </div> */}
                    </header>
                    
                    <Switch>
                        {
                            ROUTES.map((route, index) => (
                                <Route
                                    key={index}
                                    {...route}
                                />
                            ))
                        }
                    </Switch>
                </div>
            </> 
        );
    }
}

const mapStateToProps = (state) => ({
    list: state.ganreListReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    getAllFilms: (lang) => {
        dispatch( getGanreListPromise(lang) );
    }
})

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(App));
