import React from 'react';
import App from './App';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { createIntl, createIntlCache, RawIntlProvider } from 'react-intl';

import translations from '../translations';
import configDefaults from '../translations/config';
import { CHANGE_LANG } from '../reducers/L10nReducer';


const cache = createIntlCache()


class IntlComponent extends React.Component{
    state = {
        locale: this.props.match.params.locale
    }

    componentDidMount(){
        this.props.changeLang();
    }

    componentDidUpdate() {
        if (this.state.locale !== this.props.match.params.locale) {
            this.setState({
                locale: this.props.match.params.locale
            });
            this.props.changeLang();
        }

    }

    render = () => {
        let { match } = this.props;
        let locale = match.params.locale;

        let config = createIntl({
            locale,
            defaultLocale: configDefaults.defaultLanguage,
            messages: translations[locale]
        }, cache)

        return(
            <RawIntlProvider 
                key={locale} 
                value={config}
            >
                <Route component={App} />
            </RawIntlProvider>
        );
    }
}

const mapStateToProps = (state , ownProps ) => {
    console.log('stp', ownProps, state );
    return({
        translations: state.l10nReducer.translation[ ownProps.match.params.locale ],
        lang: state.l10nReducer.lang
    })
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    changeLang: () => {
        console.log('dtp', ownProps );
        dispatch({ type: CHANGE_LANG, lang: ownProps.match.params.locale });
    }
});


export default connect(mapStateToProps, mapDispatchToProps )(IntlComponent);
