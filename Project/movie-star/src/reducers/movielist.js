import {
    GET_MOVIE_LIST_REQ,
    GET_MOVIE_LIST_RES
} from '../actions';

const movieInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: []
};

const movieListReducer = ( state = movieInitialState, action) => {
    switch( action.type ){

        case GET_MOVIE_LIST_REQ: 
            return {
                ...state,
                loaded: false,
                loading: true
            }

        case GET_MOVIE_LIST_RES:
            return{
                ...state,
                loaded: true,
                loading: false,
                data: action.payload
            }

        default:
            return state;
    }
}

export default movieListReducer;