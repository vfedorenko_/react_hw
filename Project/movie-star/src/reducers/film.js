import {
    GET_FILM_REQ,
    GET_FILM_RES
} from '../actions';

const filmInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: []
};

const filmListReducer = ( state = filmInitialState, action) => {
    switch( action.type ){

        case GET_FILM_REQ: 
            return {
                ...state,
                loaded: false,
                loading: true
            }

        case GET_FILM_RES:
            return{
                ...state,
                loaded: true,
                loading: false,
                data: action.payload
            }

        default:
            return state;
    }
}

export default filmListReducer;