import translations from '../translations';

export const CHANGE_LANG = 'CHANGE_LANG'

const langInitialState = {
    lang: 'en',
    translation: translations['en']
};

const l10nReducer = ( state = langInitialState, action) => {
    switch( action.type ){

        case CHANGE_LANG: 
            return {
                ...state,
                lang: action.lang,
                translation: translations[ action.lang ]
            }

        default:
            return state;
    }
}

export default l10nReducer;