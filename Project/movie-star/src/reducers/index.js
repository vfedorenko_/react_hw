import { combineReducers } from 'redux';

import movieListReducer from './movielist';
import ganreListReducer from './genrelist';
import pageReducer from './pagination';
import filmListReducer from './film';
import l10nReducer from './L10nReducer';

const reducer = combineReducers({
    movieListReducer,
    ganreListReducer,
    pageReducer,
    filmListReducer,
    l10nReducer,
});

export default reducer;