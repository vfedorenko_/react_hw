
// export const GET_PAGE = 'GET_PAGE';
export const SET_PAGE = 'SET_PAGE';
// export const PAGE_INIT = 'PAGE_INIT';
// export const ON_WHO = 'ON_WHO';

const INIT_PAGE = 1;

const pageInitialState = {
    who: '',
    page: 1,
    total: 1
}

const pageReducer = ( state = pageInitialState, action ) => {
    switch (action.type) {
        case SET_PAGE:
            if (action.payload.who !== state.who) {
                return {
                    ...state,
                    page: INIT_PAGE,
                    who: action.payload.who,
                    total: action.payload.total
                }
            } else {
                return {
                    ...state,
                    page: action.payload.page,
                }
            }
            
        default: 
            return state
    }
}

export default pageReducer;