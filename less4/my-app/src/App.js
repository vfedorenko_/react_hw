import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch, NavLink, } from 'react-router-dom';
import { Routes } from './components/routes';

class App extends React.Component {
	// state = {
	// 	routes: Routes
	// }

	render = () => {
		// const { routes } = this.state;

		return(
			<BrowserRouter>
				<header>
					<NavLink to="/">Home Page</NavLink>
					<NavLink to="/list">List</NavLink>
					<NavLink to="/about">About</NavLink>
					<NavLink to="/contacts">Contacts</NavLink>
				</header>	
				<Switch>
					{
						Routes.map( (route, index) => (
							<Route 
								key={index}
								{...route}
							/>
						))
					}
				</Switch>
			</BrowserRouter>
		)
	}
}

export default App;