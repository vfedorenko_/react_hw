import React from 'react';
import { Link } from 'react-router-dom';


class List extends React.Component {

    state = {
        loaded: false,
        data: []
    }

    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/users')
        .then( res => res.json())
        .then( res => this.setState({
            loaded: true,
            data: res
        }))
    }

    render = () => {
        const { loaded, data} = this.state;

        return(
            <div>
                <h1>List</h1>
                {
                    loaded && (
                        <ul>
                            {
                                data.map( user => (
                                    <li key={user.id}>
                                        <Link to={`/list/:${user.id}`} >
                                            { user.name }
                                        </Link>
                                    </li>
                                ))
                            }
                        </ul>
                    )
                }
            </div>
        )
    }
    
}

export default List;


