import React from 'react';


// const ListItem = ({ match, location}) => {
//     console.log(match, location)
// }

class ListItem extends React.Component {
    state = {
        loaded: false,
        data: [],
        id: this.props.match.params.itemid[1]
    }

    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/users/' + this.state.id)
        .then( res => res.json())
        .then( res => this.setState({
            loaded: true,
            data: res
        }))
    }
    
    render = () => {
        const {loaded, data} = this.state;
        
        return(
            <div>
                <h1>List</h1> 
                    {
                        loaded && (
                            <ul>
                                <li >name: {data.name}</li>
                                <li >username: {data.name}</li>
                                <li >email: {data.email}</li>
                                <li >phone: {data.phone}</li>
                                <li >website: {data.website}</li>
                            </ul>
                        )
                    }
            </div>
        )
    }
}

export default ListItem;