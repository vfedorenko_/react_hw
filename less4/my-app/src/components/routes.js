import HomePage from './pages/homepage';
import ListWrapper from './pages/listWrapper';
import About from './pages/about';
import Contacts from './pages/contacts';
import NotFound from './pages/notfound';

export const Routes = [
    {
        path: '/',
        exact: true,
        component: HomePage
    },
    {
        path: '/list',
        component: ListWrapper
    },
    {
        path: '/about',
        exact: true,
        component: About
    },
    {
        path: '/contacts',
        exact: true,
        component: Contacts
    },
    {
        component: NotFound
    }
]
