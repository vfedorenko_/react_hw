import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch, NavLink, } from 'react-router-dom';
import { ROUTES } from './components/routes';

class App extends React.Component {
    state = {}

    render() {
        return (
            <BrowserRouter>
                <header>
                    <NavLink to="/">Home Page</NavLink>
                    <NavLink to="/posts/limit/:30">List</NavLink>
                    <NavLink to="/posts">Post</NavLink>
                </header>	
                <Switch>
                    {
                        ROUTES.map( (route, index) => (
                            <Route 
                                key={index}
                                {...route}
                            />
                        ))
                    }
                </Switch>
            </BrowserRouter>
        );
    }
}

export default App;
