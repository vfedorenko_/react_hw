import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ListItem from './listitem'


const PostsLimit = () => (
    <Switch>
        <Route exact path="/posts/limit/:listid" component={ListItem} />
    </Switch>
)

export default PostsLimit;