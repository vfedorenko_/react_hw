import React from 'react';
import { Link } from 'react-router-dom';

class ListItem extends React.Component {
        state = {
            posts: [],
            loaded: false,
            id: this.props.match.params.listid.split(':')[1]
        }
    
        componentDidMount() {
            fetch('https://jsonplaceholder.typicode.com/posts')
            .then(res => res.json() )
            .then(res => //console.log(res) )
                this.setState({
                    posts: res,
                    loaded: true
            }))
        }
    
        render() {
            let {loaded, id} = this.state;
            const {show} = this;
    
            return(
                <div>
                    <h1>List</h1>
                    {
                        loaded && show(id)
                    }
                </div>
            )
        }
    
        show = ( num ) => {
            const { posts } = this.state;
    
            return(
                <>
                    {
                        posts.map( (post, index) => {
                            if ( index < num ) {
                                return (
                                    <Link
                                        to={"/posts/:" + post.id}
                                    >
                                        <h6>{post.title}</h6>
                                        <p>{post.body}</p>
                                    </Link>
                                )
                            }
                        } )
                    }
                </>
                
            )
        }
}

export default ListItem; 


