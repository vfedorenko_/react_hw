import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PostsLimit from './postslimit';
import PostId from './postid'


const Posts = () => (
    <Switch>
        <Route exact path="/posts/:itemid" component={PostId} />
        <Route path="/posts/limit/" component={PostsLimit} />
    </Switch>
)

export default Posts;