import React from 'react';

class PostId extends React.Component {

    state = {
        loaded: false,
        loadedComments: false,
        data: [],
        comment: [],
        pressed: false,
        id: this.props.match.params.itemid.split(':')[1]
    }

    componentDidMount() {
        console.log(this.state.id)
        fetch('https://jsonplaceholder.typicode.com/posts/' + this.state.id)
        .then( res => res.json())
        .then( res => //console.log(res) )
            this.setState({
                loaded: true,
                data: res
        }))
    }

    componentDidUpdate() {
        console.log('ComponentDidUpdate')
        if (this.state.pressed) {
            fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
            .then( res => res.json())
            .then( res => //console.log(res) )
                this.setState({
                    loadedComments: true,
                    comment: res,
                    pressed: false
            }))
        }
        
    }

    loadComm = () => {
        console.log('pressed')
        this.setState ({
            pressed: true
        })
        console.log(this.state.pressed)
    }

    render() {
        const { loaded, data, loadedComments, comment } = this.state;
        const { loadComm } = this;

        return(
            <>
                <h1>Post</h1>
                {
                    loaded ? <>
                        <h2>{data.title}</h2>
                        <p>{data.body}</p>
                    </> : <h6>Loading...</h6>
                }
                {
                    loadedComments ? <ul>
                        {
                            comment.map( (item, index) => {
                                return (<li
                                    key={index}
                                >
                                    {item.body}
                                </li>)
                            } )
                        }
                    </ul> : <button onClick={loadComm}>Comments</button>
                }
                
            </>
        )
    }
    
}

export default PostId; 