import React from 'react';
import { Link } from 'react-router-dom';


class Home extends React.Component {
    state = {
        posts: [],
        loaded: false,
        num: 20,
        step: 20
    }

    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/posts')
        .then(res => res.json() )
        .then(res => //console.log(res) )
            this.setState({
                posts: res,
                loaded: true
        }))
    }

    componentDidUpdate (prevProps, prevState) {
        console.log(prevState.num, this.state.num)
        if(prevState.num !== this.state.num) {
            this.render()
        }
    }

    render() {
        let {loaded} = this.state;
        const {show} = this;

        return(
            <div>
                <h1>Home</h1>
                {
                    loaded && show()
                }
            </div>
        )
    }

    more = () => {
        let shift = this.state.num + this.state.step
        this.setState({
            num: shift
        })
        console.log(shift)
    }

    show = ( ) => {
        const { posts, num } = this.state;
        const { more } = this;

        return(
            <>
                {
                    posts.map( (post, index) => {
                        if ( index < num ) {
                            return (
                                <Link
                                    to={"/posts/:" + post.id}
                                >
                                    <h6>{post.title}</h6>
                                    <p>{post.body}</p>
                                </Link>
                            )
                        }
                    } )
                }
                {
                    (posts.length > num) &&  <button onClick={more}>Show more</button> 
                }
            </>
            
        )
    }
}

export default Home;