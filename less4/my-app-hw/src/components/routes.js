import Home from './pages/home';
import Posts from './pages/post';
import NotFound from './pages/notfound';

export const ROUTES = [
    {
        path: '/',
        component: Home,
        exact: true
    },
    {
        path: '/posts/',
        component: Posts,
        exact: false
    },
    {
        component: NotFound
    }
]

export default ROUTES;