import NotFound from './pages/notfound';
import Home from './pages/home';
import Post from './pages/post';
import User from './pages/user';

export const ROUTES = [
    {
        path: '/:langPath/',
        component: Home,
        exact: true
    },
    {
        path: '/:langPath/post',
        component: Post,
        exact: false
    },
    {
        path: '/:langPath/user',
        component: User,
        exact: false
    },
    {
        component: NotFound
    }
]

export default ROUTES;