import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import L10nWrapper from './L10nWrapper';
import App from './App';
import store from '../redux/store';

const Wrapper = () => (
    <Provider store={store}>
        <BrowserRouter>
            <L10nWrapper />
        </BrowserRouter>
    </Provider>
);

export default Wrapper;