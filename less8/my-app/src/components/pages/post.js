import React from 'react';
import { Switch } from 'react-router-dom';
import L10nRoute from '../common/L10nRoute';
import PostId from './postid.js'


const Posts = () => (
    <Switch>
        <L10nRoute exact path="/:langPath/post/:itemid" component={PostId} />
    </Switch>
)

export default Posts;