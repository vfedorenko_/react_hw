import React from 'react';
import { Switch } from 'react-router-dom';
import L10nRoute from '../common/L10nRoute';
import UserId from './userid.js'


const User = () => (
    <Switch>
        <L10nRoute exact path="/:langPath/user/:userid" component={UserId} />
    </Switch>
)

export default User;