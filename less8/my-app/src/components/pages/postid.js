import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { FormattedMessage, injectIntl } from 'react-intl';

import L10Link from '../common/L10nLink';

import { getPostPromise } from '../../actions/post';
import { getCommentPromise } from '../../actions/comment';

import favicon2 from '../images/favicon_smile.ico?v2';

class PostId extends React.Component {

    componentDidMount() {
        let id = this.props.match.params.itemid.split(':')[1];
        console.log(id)
        this.props.getPost(id);
    }

    loadComm = () => {
        let id = this.props.match.params.itemid.split(':')[1];
        console.log(id)
        this.props.getComment(id);
    }

    render() {
        const { loaded, data } = this.props.post;
        const { comment } = this.props;
        const { loadComm } = this;

        return(
            <>
                <Helmet>
                    <title> Posts </title>
                    <link rel="shortcut icon" href={favicon2}/>
                </Helmet>
                <h1><FormattedMessage id="posts.title"/></h1>
                
                {
                    loaded ? <>
                        <L10Link 
                            key={data.id}
                            to={"/user/:" + data.userId}
                        >
                            <FormattedMessage id="posts.user"/>
                        </L10Link>
                        <h2>{data.title}</h2>
                        <p>{data.body}</p>
                    </> : <h6>Loading...</h6>
                }
                {
                    comment.loaded ? <ul>
                        {
                            comment.data.map( (item, index) => {
                                return (<li
                                    key={index}
                                >
                                    {item.body}
                                </li>)
                            } )
                        }
                    </ul> : <button onClick={loadComm}><FormattedMessage id="posts.comment"/></button>
                }
                
            </>
        )
    }
    
}

const mapStateToProps = (state) => ({
    post: state.postReducer,
    comment: state.commentReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    getPost: (id) => {
        dispatch( getPostPromise(id) );
    },
    getComment: (id) => {
        dispatch( getCommentPromise(id) )
    }
})

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(PostId));