import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { FormattedMessage, injectIntl } from 'react-intl';

import L10Link from '../common/L10nLink';

import { getUserPromise } from '../../actions/user';

import favicon2 from '../images/favicon_home.ico?v2';

class UserId extends React.Component {

    componentDidMount() {
        let id = this.props.match.params.userid.split(':')[1];
        console.log(id)
        this.props.getUser(id);
    }

    render() {
        const { loaded, data } = this.props.user;

        return(
            <>
                <Helmet>
                    <title> Posts </title>
                    <link rel="shortcut icon" href={favicon2}/>
                </Helmet>
                <h1><FormattedMessage id="user.main"/></h1>
                
                <h2><FormattedMessage id="user.id"/> {this.props.match.params.userid}</h2>
                {
                    loaded && 
                        data.map( (post, index) => {
                            return (
                                    <L10Link
                                        key={post.id}
                                        to={"/post/:" + post.id}
                                    >
                                        <h6>{post.title}</h6>
                                        <p>{post.body}</p>
                                    </L10Link>
                                )
                        } )
                }
            </>
        )
    }
    
}

const mapStateToProps = (state) => ({
    user: state.userReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    getUser: (id) => {
        dispatch( getUserPromise(id) );
    }
})

export default injectIntl(connect( mapStateToProps, mapDispatchToProps )(UserId));