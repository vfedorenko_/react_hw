import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { FormattedMessage, injectIntl } from 'react-intl';

import L10Link from '../common/L10nLink';

import { getPostsPromise } from '../../actions/posts';

import favicon2 from '../images/favicon_red.ico?v2';


class Home extends React.Component {
    state = {
        num: 50
    }

    componentDidMount() {
        this.props.getAllPosts();
    }

    render() {
        console.log(this.props)
        let { num } = this.state;
        let { data } = this.props.posts;
        const { more } = this;

        return(
            <div>
                <Helmet>
                    <title> 
                        Home 
                    </title>
                    <link rel="shortcut icon" href={favicon2}/>
                </Helmet>
                <h1><FormattedMessage id="home.title"/></h1>
                {
                    data.map( (post, index) => {
                        if ( index < num ) {
                            return (
                                <L10Link
                                    key={post.id}
                                    to={"/post/:" + post.id}
                                >
                                    <h6>{post.title}</h6>
                                    <p>{post.body}</p>
                                </L10Link>
                            )
                        }
                    } )
                }
                {
                    (data.length > num) &&  <button onClick={more}><FormattedMessage id="home.more"/></button> 
                }
            </div>
        )
    }

    more = () => {
        let shift = this.state.num + 25;
        this.setState({
            num: shift
        })
        console.log(shift)
    }
}

const mapStateToProps = (state) => ({
    posts: state.postsReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    getAllPosts: () => {
        dispatch( getPostsPromise() );
    }
})

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Home));