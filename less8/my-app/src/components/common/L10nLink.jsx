import React from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const L10nLink = ({
    to,
    lang,
    children,
    ...props
}) => {

let langPath;

if( typeof(to) === 'object'){
    langPath = {
        ...to,
        pathname: `/${lang}${to.pathname}`
    }
} else {
    langPath = `/${lang}${to}`
}

return(
    <Link 
    to={langPath} 
    {...props}> 
        {children}
    </Link>
)

}

const mapStateToProps = (state) => ({
lang: state.localization.lang
})

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(L10nLink);