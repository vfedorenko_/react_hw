import React from 'react'
import { Route } from 'react-router-dom';

import config from '../config';

const L10nRoute = ({
        path,
        lang,
        children,
        ...props
    }) => {
    // :::

    return(
        <Route 
            path={`/:locale(${config.supprotedLangs.join('|')})${path}`} 
            {...props}> 
                {children}
        </Route>
    )

}


export default L10nRoute;