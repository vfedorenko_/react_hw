import React from 'react';
import App from './App';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
/*
    Localization Support
*/
import { createIntl, createIntlCache, RawIntlProvider } from 'react-intl';

import translations from '../translation';


const cache = createIntlCache()


class IntlComponent extends React.Component{

    state = {
        localeConfig: null
    }

    componentDidMount(){
        this.props.changeLang();
    }

    render = () => {
        let { match } = this.props;
        let locale = match.params.locale;

        let config = createIntl({
            locale,
            defaultLocale: 'uk',
            messages: translations[locale]
        }, cache)

        return(
            <RawIntlProvider 
                key={locale} 
                value={config}
            >
                <Route component={App} />
            </RawIntlProvider>
        );
    }
}

const mapStateToProps = (state , ownProps ) => {
    // console.log('stp', ownProps, state );
    return({
        translations: state.localization.translation[ ownProps.match.params.locale ],
        lang: state.localization.lang
    })
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    changeLang: () => {
        dispatch({ type: 'CHANGE_LANG', lang: ownProps.match.params.locale });
    }
});


export default connect(mapStateToProps, mapDispatchToProps )(IntlComponent);
