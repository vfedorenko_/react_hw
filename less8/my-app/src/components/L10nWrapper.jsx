import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import intlComponent from './intl.container';

import config from './config';
// Schema for variable in route (uk|ru|en)
const SupportedLangs = config.supprotedLangs.join('|');

const LocalizationComponent = () => {
    return(
        <Switch>
            <Route path={`/:locale(${SupportedLangs})`} component={intlComponent} />
            <Redirect to={`${config.defaultLanguage}`} />
        </Switch>
    )
}

export default LocalizationComponent;