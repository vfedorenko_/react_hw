import React from 'react';
import './App.css';
import { Switch } from 'react-router-dom';
import { FormattedMessage, injectIntl } from 'react-intl';

import L10nNavLink from './common/L10nNavLink';
import L10nRoute from './common/L10nRoute';

import { ROUTES } from './routes'


const App = () => (

    <>
        <nav>
            <L10nNavLink to="/">
                <FormattedMessage id="common.main"/>
            </L10nNavLink>
        </nav>
        <Switch>
            {
                ROUTES.map( ( route, index ) => (
                    <L10nRoute 
                        key={index}
                        { ...route }
                    />
                ))
            }
        </Switch>
    </>
    
)


export default injectIntl( App );
