import axios from 'axios';

export const axios_posts = axios.create({
    baseURL: 'http://jsonplaceholder.typicode.com/posts/',
    headers: {
        'X-Custom-Header': 'foobar'
    }
});

export const axios_users = axios.create({
    baseURL: 'http://jsonplaceholder.typicode.com/',
    headers: {
        'X-Custom-Header': 'foobar'
    }
});