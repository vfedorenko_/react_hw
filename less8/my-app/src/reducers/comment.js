import {
    GET_COMENT_REQ,
    GET_COMENT_RES
} from '../actions';

const postsInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: []
};

const commentReducer = ( state = postsInitialState, action) => {
    switch( action.type ){

        case GET_COMENT_REQ: 
            return {
                ...state,
                loaded: false,
                loading: true
            }

        case GET_COMENT_RES:
            return{
                ...state,
                loaded: true,
                loading: false,
                data: action.payload
            }

        default:
            return state;
    }
}

export default commentReducer;