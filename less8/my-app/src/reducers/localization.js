import translations from '../translation';

const postsInitialState = {
    lang: 'uk',
    translation: translations['uk']
};

const translationReducer = ( state = postsInitialState, action) => {
    switch( action.type ){

        case 'CHANGE_LANG': 
            return {
                ...state,
                lang: action.lang,
                translation: translations[ action.lang ]
            }

        default:
            return state;
    }
}

export default translationReducer;