import { combineReducers } from 'redux';

import postsReducer from './posts';
import postReducer from './post';
import commentReducer from './comment';
import userReducer from './user';
import localization from './localization';

const reducer = combineReducers({
    postsReducer,
    postReducer,
    commentReducer,
    userReducer,
    localization
});

export default reducer;