import {
    GET_USER_REQ,
    GET_USER_RES
} from '../actions';

const postsInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: []
};

const userReducer = ( state = postsInitialState, action) => {
    switch( action.type ){

        case GET_USER_REQ: 
            return {
                ...state,
                loaded: false,
                loading: true
            }

        case GET_USER_RES:
            return{
                ...state,
                loaded: true,
                loading: false,
                data: action.payload
            }

        default:
            return state;
    }
}

export default userReducer;