import {
    GET_POSTS_REQ,
    GET_POSTS_RES
} from '../actions';

const postsInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: []
};

const postsReducer = ( state = postsInitialState, action) => {
    switch( action.type ){

        case GET_POSTS_REQ: 
            return {
                ...state,
                loaded: false,
                loading: true
            }

        case GET_POSTS_RES:
            return{
                ...state,
                loaded: true,
                loading: false,
                data: action.payload
            }

        default:
            return state;
    }
}

export default postsReducer;