import { axios_users } from '../helpers/axiosnight';

export const GET_USER_REQ = 'GET_USER_REQ';
export const GET_USER_RES = 'GET_USER_RES';
export const GET_USER_ERROR = 'GET_USER_ERROR';
export const PROMISE = 'PROMISE';

export const getUserPromise = ( num ) => ( dispatch ) => {

    dispatch({
        type: PROMISE,
        actions: [ GET_USER_REQ, GET_USER_RES, GET_USER_ERROR ],
        promise: axios_users.get(`posts?userId=${num}`)
    });

}