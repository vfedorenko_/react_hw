import { axios_posts } from '../helpers/axiosnight';

export const GET_POSTS_REQ = 'GET_POSTS_REQ';
export const GET_POSTS_RES = 'GET_POSTS_RES';
export const GET_POSTS_ERROR = 'GET_POSTS_ERROR';
export const PROMISE = 'PROMISE';

export const getPostsPromise = () => ( dispatch ) => {

    dispatch({
        type: PROMISE,
        actions: [ GET_POSTS_REQ, GET_POSTS_RES, GET_POSTS_ERROR ],
        promise: axios_posts.get('')
    })
}