import { axios_posts } from '../helpers/axiosnight';

export const GET_COMENT_REQ = 'GET_COMENT_REQ';
export const GET_COMENT_RES = 'GET_COMENT_RES';
export const GET_COMENT_ERROR = 'GET_COMENT_ERROR';
export const PROMISE = 'PROMISE';

export const getCommentPromise = ( num ) => ( dispatch ) => {

    dispatch({
        type: PROMISE,
        actions: [ GET_COMENT_REQ, GET_COMENT_RES, GET_COMENT_ERROR ],
        promise: axios_posts.get(`${num}/comments`)
    });

}