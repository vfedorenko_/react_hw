import { axios_posts } from '../helpers/axiosnight';

export const GET_POST_REQ = 'GET_POST_REQ';
export const GET_POST_RES = 'GET_POST_RES';
export const GET_POST_ERROR = 'GET_POST_ERROR';
export const PROMISE = 'PROMISE';

export const getPostPromise = ( num ) => ( dispatch ) => {

    dispatch({
        type: PROMISE,
        actions: [ GET_POST_REQ, GET_POST_RES, GET_POST_ERROR ],
        promise: axios_posts.get(num)
    });

}