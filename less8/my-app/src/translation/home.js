export default {
    uk: {
        'home.title': 'Головна сторінка',
        'home.more': 'Показати ще',
    },
    en: {
        'home.title': 'Home page',
        'home.more': 'vedere di più',
    },
    it: {
        'home.title': 'Prima pagina',
        'home.more': 'Show more',
    },
    de: {
        'home.title': 'Home page',
        'home.more': 'Showen',
    }
}