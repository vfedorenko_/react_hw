export default {
    uk: {
        'posts.title': 'Пост',
        'posts.user': 'Переглянути всі пости користувача',
        'posts.comment': 'Коментарі',
    },
    en: {
        'posts.title': 'Post',
        'posts.user': "Show all user's posts",
        'posts.comment': 'Comments',
    },
    it: {
        'posts.title': 'Il post',
        'posts.user': "vedi più posts dal'user",
        'posts.comment': 'i commenti',
    },
    de: {
        'posts.title': 'Der Post',
        'posts.user': 'Showen die Usar',
        'posts.comment': 'Dei Komments',
    }
}