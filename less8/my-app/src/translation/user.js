export default {
    uk: {
        'user.main': 'Головна сторінка користувача',
        "user.id": 'Користувач: ',
    },
    en: {
        'user.main': "User's id: ",
        "user.id": 'Користувач: ',
    },
    it: {
        'user.main': "Usar: ",
        "user.id": 'Користувач: ',
    },
    de: {
        'user.main': 'ЧШ',
        "user.id": 'User: ',
    }
}