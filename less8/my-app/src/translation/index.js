import Common from './navigation';
import Home from './home';
import Post from './post';
import User from './user';

import config from '../components/config';

const TranslationsArray = [Common, Home, Post, User];

let ConnectedTranslations = {};
config.supprotedLangs.forEach( lang => ConnectedTranslations[lang] = {} );

TranslationsArray.forEach( translationObject => {
    config.supprotedLangs.forEach( lang => {
        ConnectedTranslations[lang] = {
        ...ConnectedTranslations[lang],
        ...translationObject[lang]
        };
    })
});

export default ConnectedTranslations;