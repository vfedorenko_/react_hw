import React, { Component } from 'react';
// import ReactDOM from 'react-dom';
import Button from './component/button';
import Wrapper from './component/compwrapper';
import MyImg from './component/img';
import Cell from './component/cell';
import Row from './component/row';
import Table from './component/table';
import './App.css';

const url = "http://www.json-generator.com/api/json/get/cpwBTWhPEy?indent=2";

// const buttclick = ( e ) => {
// 	const element = React.createElement('h1', {}, 'Hello man!');
// 	const root = document.getElementById('root');
// 	ReactDOM.render( element, root )

// }

// const fetchData = ( url ) => {
// 	let data = [];
	

// 	return data;
// }

class App extends Component {

	state = {
		loaded: false,
		user: []
	}

	changeState = ( e ) => {
		// console.log(e.target)
		const id = Number(e.target.dataset.id);
		// console.log(id)

		let chUser = this.state.user.map( item => {
			if (item.index === id) {
				item.interviewed = !item.interviewed;
			}
			return item; 
		} )
		this.setState({
			user: chUser
		})
	}

	componentDidMount = () => {
		fetch(url)
			.then( res => res.json() )
			.then( res => { 
					let data = res.map( item => {
						let it = item;
						it.interviewed = false;
						return it;
					})
					console.log(data)
					this.setState({
						user: data,
						loaded: true
					} );
			} ); 
	}

	render = () => {
		const { user, loaded } = this.state;
		const { changeState } = this;

		return (
			<div className="App">
				<header className="App-header">
					<MyImg 
						url={"https://media.springernature.com/lw630/nature-cms/uploads/cms/pages/2913/top_item_image/cuttlefish-e8a66fd9700cda20a859da17e7ec5748.png"}
						className="App-logo" 
						alt="logo" 
					/>
						{
							loaded && (
								<ul>
									{
										user.map( u => (
											<Wrapper
												key={u.index}
												child1={u.name}
												child2={<Button 
													index={u.index}
													title="Press me"
													style={{background: u.interviewed ? 'red' : 'green'}}
													handler={changeState} />
												}
											/>
										))
									}
								</ul>
							)
						}
				</header>
				<div>
					<Table>
						<Row head="true">
							<Cell type="" background="red">#</Cell>
							<Cell type="date">2</Cell>
							<Cell type="number">3</Cell>
							<Cell type="money" currency="$">4$</Cell>
						</Row>
						<Row>
							<Cell type="" background="red">1</Cell>
							<Cell type="date">2</Cell>
							<Cell type="number">3</Cell>
							<Cell type="money" currency="$">4$</Cell>
						</Row>
					</Table>
				</div>
			</div>
		);
	}
	
}

export default App;
