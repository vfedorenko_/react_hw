import React from 'react';

const Cell = ( { type, span, bg, color } ) => {
    return(
        <td 
            className={type}
            colSpan={span}
            style={ {
                background: {bg},
                color: {color} } 
            }
        >
            
        </td>
    )
}

Cell.defaultProps = {
    type: 'text',
    span: 1,
    bg: 'transparent',
    color: 'black'
}

export default Cell;