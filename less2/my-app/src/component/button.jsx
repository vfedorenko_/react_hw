//class work task 1

import React from 'react';

const Button = ( { title, handler, style, index } ) => {
    return(
        <button
            data-id={index}
            style={style}
            onClick={handler}
        >
            { title || 'Hello!' }
        </button>
    )
}

export default Button;