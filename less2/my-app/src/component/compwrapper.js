import React from 'react';

const Wrapper = ( { child1, child2, style } ) => (
        <li
            className={style}
        >
            { child1 }
            { child2 }
        </li>
    )

export default Wrapper;