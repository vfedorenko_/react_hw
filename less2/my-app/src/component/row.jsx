import React from 'react';

const Row = ( { head, children } ) => {
    return(
        <tr
            head={head}
        >
            {
                children
            }
        </tr>
    )
}

export default Row;

Row.defaultProps = {
    head: false
}