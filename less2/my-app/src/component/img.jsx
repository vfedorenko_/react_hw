import React, { Component } from 'react';


class MyImg extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url: props.url,
            loaded: false,
            className: props.style,
            img: ''
        }
    }

	componentDidMount = () => {
        fetch( this.state.url )
            .then( res => {
                this.setState({
                    img: res,
                    loaded: true
                })
            });
    }

    render = () => {
        const { img, loaded, className } = this.state

        return(
            <div id="image">
                {
                    (loaded) ? (
                        <h1>Hello Strange</h1>
                    ) : (
                        <img src={img} className={className} alt="new image" />
                    )
                }
            </div>
        )
    }
}

export default MyImg;